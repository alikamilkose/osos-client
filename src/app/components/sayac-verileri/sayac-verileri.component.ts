import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Sayac } from './sayac';
import { SayacVerileriService } from './sayac-verileri.service';

@Component({
  selector: 'app-sayac-verileri',
  templateUrl: './sayac-verileri.component.html'
})
export class SayacVerileriComponent implements OnInit {

  sistemKullanicilari: SelectItem[] = [];
  lisansTurleri: SelectItem[] = [];
  tesisler: SelectItem[] = [];
  sayaclar: Sayac[] = [];

  selectedKullanici: any;
  selectedLisansTuru: any;
  selectedTesisler: any;
  constructor(private sayacVerileriService: SayacVerileriService) { }

  ngOnInit(): void {
    this.getLisansTurleri();
  }

  getLisansTurleri() {
    this.sayacVerileriService.getLisansTurleri().subscribe(result => {
      result.forEach(element => {
        this.lisansTurleri.push({ label: element.name, value: element });
      });
    });
  }

  getSistemKullanicilari(lisansTuru: any) {
    this.sayacVerileriService.getSistemKullanicilariByLisansTuru(lisansTuru).subscribe(result => {
      result.forEach(element => {
        this.sistemKullanicilari.push({ label: element.ad, value: element });
      });
    });
  }

  kullaniciChange() {
    this.tesisler = [];
    this.sayaclar = [];
    this.sayacVerileriService.getTesislerByKullanici(this.selectedKullanici.id).subscribe(result => {
      result.forEach(element => {
        this.tesisler.push({ label: element.ad, value: element });
      });
    });
  }

  lisansTuruChange() {
    this.sistemKullanicilari = [];
    this.tesisler = [];
    this.sayaclar = [];
    this.getSistemKullanicilari(this.selectedLisansTuru)
  }

  tesisChange() {
    this.sayaclar = [];
    this.sayacVerileriService.getSayaclarByTesis(this.selectedTesisler.id).subscribe(result => {
      result.forEach(element => {
        this.sayaclar = result;
      })
    })
  }
}
