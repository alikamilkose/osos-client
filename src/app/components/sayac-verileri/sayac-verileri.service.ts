import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SayacVerileriService {

  constructor(private http: HttpClient) { }

  getLisansTurleri(): Observable<any> {
    return this.http.get(`${environment.API_URL}/referanslar/lisansturleri`);
  }

  getSistemKullanicilariByLisansTuru(lisansTuru: any): Observable<any> {
    return this.http.post(`${environment.API_URL}/kullanici/bylisansturu`, lisansTuru);
  }

  getTesislerByKullanici(kullaniciId: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/tesis/bykullanici/${kullaniciId}`);
  }

  getSayaclarByTesis(tesisId: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/sayac/bytesis/${tesisId}`);
  }
}
