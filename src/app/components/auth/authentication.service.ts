import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../login/login';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelperService) { }

  authenticate(credintials: Login): Observable<any> {
    return this.http.post(`${environment.API_URL}/authenticate`, credintials);
  }

  isAuthenticated(): boolean{
    const token = this.getToken();
    return(token != null && !this.jwtHelper.isTokenExpired(token))

  }
  getToken(): string{
    return sessionStorage.getItem('token');
  }
}

